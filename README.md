# Redmine plugin public project permission

A Redmine plugin to control public project's permission by user's
custom field value.

You can disallow access to public projects from some non members and
anonymous by user's custom field value. This can be implemented by
roles. But implementation by roles needs to use a new role instead of
the built-in non members role. It means that the target users are
joined as members to the public projects. It causes the followings:

  * The target users are listed in the public projects' overview page.

  * The "For any event on all my projects" email notification doesn't work.

  * ...

## Install

Install this plugin:

```bash
cd redmine
git clone https://gitlab.com/redmine-plugin-public-project-permission/redmine-plugin-public-project-permission.git plugins/public_project_permission
```

Restart Redmine.

## Uninstall

Uninstall this plugin:

```bash
cd redmine
rm -rf plugins/public_project_permission
```

Restart Redmine.

## Usage

Create a new custom field for users with the following configurations:

  * Name: (You can choose your favorite name)
  * Format: `Boolean`

Open plugin settings page:

https://redmine.example.com/settings/plugin/public_project_permission

Specify the name of the created custom field for this plugin.

## Authors

  * Sutou Kouhei `<kou@clear-code.com>`

## License

GPLv2 or later. See [LICENSE](LICENSE) for details.

