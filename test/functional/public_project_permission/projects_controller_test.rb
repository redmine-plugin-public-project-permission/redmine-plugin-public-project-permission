# Copyright (C) 2021  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require File.expand_path("../../../test_helper", __FILE__)

module PublicProjectPermission
  class ProjectsControllerTest < Redmine::ControllerTest
    tests ProjectsController

    fixtures :custom_fields
    fixtures :custom_values
    fixtures :member_roles
    fixtures :members
    fixtures :projects
    fixtures :roles
    fixtures :users

    def setup
      @request.session[:user_id] = nil
      @public_project = Project.where(is_public: true).first
      @public_project_non_member =
        User
          .where(admin: false)
          .where.not(id: @public_project.users)
          .first
      @public_project_member =
        @public_project.users.where(admin: false).first
      @private_project = Project.where(is_public: false).first
      @private_project_non_member =
        User
          .where(admin: false)
          .where.not(id: @private_project.users)
          .first
      @private_project_member =
        @private_project.users.where(admin: false).first
      @admin = User.where(admin: true).first
      @custom_field = UserCustomField.generate!(field_format: "bool")
    end

    def set_custom_value(user, value)
      sql_value = value ? "1" : "0"
      custom_value =
        user
          .custom_values
          .where(custom_field: @custom_field)
          .first
      if custom_value
        custom_value.value = sql_value
        custom_value.save!
      else
        user.custom_values.create!(custom_field: @custom_field,
                                   value: sql_value)
      end
    end

    def test_index_no_setting_anonymous
      get :index
      assert_response :success
      project_links = html_document.css("ul.projects li a")
      assert_equal([
                     "eCookbook",
                     "Child of private child",
                     "eCookbook Subproject 1",
                     "eCookbook Subproject 2",
                   ],
                   project_links.collect(&:text))
    end

    def test_index_no_setting_non_member
      @request.session[:user_id] = @public_project_non_member.id
      get :index
      assert_response :success
      project_links = html_document.css("ul.projects li a")
      assert_equal([
                     "eCookbook",
                     "Child of private child",
                     "eCookbook Subproject 1",
                     "eCookbook Subproject 2",
                   ],
                   project_links.collect(&:text))
    end

    def test_index_no_setting_member
      @request.session[:user_id] = @public_project_member.id
      get :index
      assert_response :success
      project_links = html_document.css("ul.projects li a")
      assert_equal([
                     "eCookbook",
                     "Private child of eCookbook",
                     "Child of private child",
                     "eCookbook Subproject 1",
                     "eCookbook Subproject 2",
                     "OnlineStore",
                   ],
                   project_links.collect(&:text))
    end

    def test_index_no_setting_admin
      @request.session[:user_id] = @admin.id
      get :index
      assert_response :success
      project_links = html_document.css("ul.projects li a")
      assert_equal([
                     "eCookbook",
                     "Private child of eCookbook",
                     "Child of private child",
                     "eCookbook Subproject 1",
                     "eCookbook Subproject 2",
                     "OnlineStore",
                   ],
                   project_links.collect(&:text))
    end

    def test_index_with_setting_anonymous
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        get :index
        assert_response :success
        project_links = html_document.css("ul.projects li a")
        assert_equal([],
                     project_links.collect(&:text))
      end
    end

    def test_index_with_setting_non_member_false
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        @request.session[:user_id] = @public_project_non_member.id
        get :index
        assert_response :success
        project_links = html_document.css("ul.projects li a")
        assert_equal([],
                     project_links.collect(&:text))
      end
    end

    def test_index_with_setting_non_member_true
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        set_custom_value(@public_project_non_member, true)
        @request.session[:user_id] = @public_project_non_member.id
        get :index
        assert_response :success
        project_links = html_document.css("ul.projects li a")
        assert_equal([
                       "eCookbook",
                       "Child of private child",
                       "eCookbook Subproject 1",
                       "eCookbook Subproject 2",
                     ],
                     project_links.collect(&:text))
      end
    end

    def test_index_with_setting_member_false
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        @request.session[:user_id] = @public_project_member.id
        get :index
        assert_response :success
        project_links = html_document.css("ul.projects li a")
        assert_equal([
                       "eCookbook",
                       "Private child of eCookbook",
                       "OnlineStore",
                     ],
                     project_links.collect(&:text))
      end
    end

    def test_index_with_setting_member_true
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        set_custom_value(@public_project_member, true)
        @request.session[:user_id] = @public_project_member.id
        get :index
        assert_response :success
        project_links = html_document.css("ul.projects li a")
        assert_equal([
                       "eCookbook",
                       "Private child of eCookbook",
                       "Child of private child",
                       "eCookbook Subproject 1",
                       "eCookbook Subproject 2",
                       "OnlineStore",
                     ],
                     project_links.collect(&:text))
      end
    end

    def test_index_with_setting_admin_false
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        @request.session[:user_id] = @admin.id
        get :index
        assert_response :success
        project_links = html_document.css("ul.projects li a")
        assert_equal([
                       "eCookbook",
                       "Private child of eCookbook",
                       "Child of private child",
                       "eCookbook Subproject 1",
                       "eCookbook Subproject 2",
                       "OnlineStore",
                     ],
                     project_links.collect(&:text))
      end
    end

    def test_index_with_setting_admin_true
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        set_custom_value(@admin, true)
        @request.session[:user_id] = @admin.id
        get :index
        assert_response :success
        project_links = html_document.css("ul.projects li a")
        assert_equal([
                       "eCookbook",
                       "Private child of eCookbook",
                       "Child of private child",
                       "eCookbook Subproject 1",
                       "eCookbook Subproject 2",
                       "OnlineStore",
                     ],
                     project_links.collect(&:text))
      end
    end

    def test_show_no_setting_public_anonymous
      get :show, params: {id: @public_project.id}
      assert_response :success
    end

    def test_show_no_setting_public_non_member
      @request.session[:user_id] = @public_project_non_member.id
      get :show, params: {id: @public_project.id}
      assert_response :success
    end

    def test_show_no_setting_public_member
      @request.session[:user_id] = @public_project_member.id
      get :show, params: {id: @public_project.id}
      assert_response :success
    end

    def test_show_no_setting_public_admin
      @request.session[:user_id] = @admin.id
      get :show, params: {id: @public_project.id}
      assert_response :success
    end

    def test_show_no_setting_private_anonymous
      get :show, params: {id: @private_project.id}
      back_url = project_url(id: @private_project.id)
      assert_redirected_to signin_path(back_url: back_url)
    end

    def test_show_no_setting_private_non_member
      @request.session[:user_id] = @private_project_non_member.id
      get :show, params: {id: @private_project.id}
      assert_response :forbidden
    end

    def test_show_no_setting_private_member
      @request.session[:user_id] = @private_project_member.id
      get :show, params: {id: @private_project.id}
      assert_response :success
    end

    def test_show_no_setting_private_admin
      @request.session[:user_id] = @admin.id
      get :show, params: {id: @private_project.id}
      assert_response :success
    end

    def test_show_with_setting_public_anonymous
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        get :show, params: {id: @public_project.id}
        back_url = project_url(id: @public_project.id)
        assert_redirected_to signin_path(back_url: back_url)
      end
    end

    def test_show_with_setting_public_non_member_false
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        @request.session[:user_id] = @public_project_non_member.id
        get :show, params: {id: @public_project.id}
        assert_response :forbidden
      end
    end

    def test_show_with_setting_public_non_member_true
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        set_custom_value(@public_project_non_member, true)
        @request.session[:user_id] = @public_project_non_member.id
        get :show, params: {id: @public_project.id}
        assert_response :success
      end
    end

    def test_show_with_setting_public_member_false
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        @request.session[:user_id] = @public_project_member.id
        get :show, params: {id: @public_project.id}
        assert_response :success
      end
    end

    def test_show_with_setting_public_member_true
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        set_custom_value(@public_project_member, true)
        @request.session[:user_id] = @public_project_member.id
        get :show, params: {id: @public_project.id}
        assert_response :success
      end
    end

    def test_show_with_setting_public_admin_false
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        @request.session[:user_id] = @admin.id
        get :show, params: {id: @public_project.id}
        assert_response :success
      end
    end

    def test_show_with_setting_public_admin_true
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        set_custom_value(@admin, true)
        @request.session[:user_id] = @admin.id
        get :show, params: {id: @public_project.id}
        assert_response :success
      end
    end

    def test_show_with_setting_private_anonymous
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        get :show, params: {id: @private_project.id}
        back_url = project_url(id: @private_project.id)
        assert_redirected_to signin_path(back_url: back_url)
      end
    end

    def test_show_with_setting_private_non_member_false
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        @request.session[:user_id] = @private_project_non_member.id
        get :show, params: {id: @private_project.id}
        assert_response :forbidden
      end
    end

    def test_show_with_setting_private_non_member_true
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        set_custom_value(@private_project_non_member, true)
        @request.session[:user_id] = @private_project_non_member.id
        get :show, params: {id: @private_project.id}
        assert_response :forbidden
      end
    end

    def test_show_with_setting_private_member_false
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        @request.session[:user_id] = @private_project_member.id
        get :show, params: {id: @private_project.id}
        assert_response :success
      end
    end

    def test_show_with_setting_private_member_true
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        set_custom_value(@private_project_member, true)
        @request.session[:user_id] = @private_project_member.id
        get :show, params: {id: @private_project.id}
        assert_response :success
      end
    end

    def test_show_with_setting_private_admin_false
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        @request.session[:user_id] = @admin.id
        get :show, params: {id: @private_project.id}
        assert_response :success
      end
    end

    def test_show_with_setting_private_admin_true
      with_settings(plugin_public_project_permission: {
                      "custom_field_name" => @custom_field.name,
                    }) do
        set_custom_value(@admin, true)
        @request.session[:user_id] = @admin.id
        get :show, params: {id: @private_project.id}
        assert_response :success
      end
    end
  end
end
